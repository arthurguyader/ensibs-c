/**
 * \file main.h
 * \brief Boolean and logical operator.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#include <stdlib.h>
#include <stdio.h>

#include "main.h"

/**
 * Make some arithmetic operation.
 */
int main(int argc, char *argv[]) {
    int x_value;
    unsigned int x_value_unsigned;

    /*
     * Opérateur AND bit-à-bit :
     *
     *    (0000 0000 0000 0000 0000 0000 0000 0101): 0x5
     *  & (0000 0000 0000 0000 0000 0000 0000 0110): 0x6
     *    =========================================
     *    (0000 0000 0000 0000 0000 0000 0000 0100): 0x4
     */
    x_value = 5 & 6;

    printf("X = 5 & 6\t// X = %d\n", x_value);

    /*
     * Opérateur booléen (logique) sur des entiers, un entier non nul est équivalent à un 1 :
     *
     * 5 && 6 = 1 (1 ET 1 donne 1)
     */
    x_value = 5 && 6;

    printf("X = 5 && 6\t// X = %d\n", x_value);

    /*
     * Opérateur OR bit-à-bit :
     *
     *    (0000 0000 0000 0000 0000 0000 0000 0101): 0x5
     *  | (0000 0000 0000 0000 0000 0000 0000 0110): 0x6
     *    =========================================
     *    (0000 0000 0000 0000 0000 0000 0000 0111): 0x7
     */
    x_value = 5 | 6;

    printf("X = 5 | 6\t// X = %d\n", x_value);

    /*
     * Opérateur booléen (logique) sur des entiers, un entier non nul est équivalent à un 1 :
     *
     * 5 || 6 = 1 (1 OU 1 donne 1)
     */
    x_value = 5 || 6;

    printf("X = 5 || 6\t// X = %d\n", x_value);

    /*
     * Opérateur XOR bit-à-bit :
     *
     *    (0000 0000 0000 0000 0000 0000 0000 0101): 0x5
     *  ^ (0000 0000 0000 0000 0000 0000 0000 0110): 0x6
     *    =========================================
     *    (0000 0000 0000 0000 0000 0000 0000 0011): 0x3
     */
    x_value = 5 ^ 6;

    printf("X = 5 ^ 6\t// X = %d\n", x_value);

    /*
     * Opération invalide.
     *
     * x_value = 5 ^^ 6;
     *
     * printf("X = 5 ^^ 6\t\t// X = %d\n", x_value);
     */

    /*
     * Opérateur NOT bit-à-bit :
     *
     *  ! (0000 0000 0000 0000 0000 0000 0000 0101): 0x5
     *    =========================================
     *    (1111 1111 1111 1111 1111 1111 1111 1010): 0xfffffffa = 4294967290 (unsigned)
     *
     * Autre exemple avec 4 :
     *
     *  ! (0000 0000 0000 0000 0000 0000 0000 0100): 0x4
     *    =========================================
     *    (1111 1111 1111 1111 1111 1111 1111 1011): 0xfffffffb = 4294967291 (unsigned)
     *
     * Entier signé ici, or :
     *
     *  0 = 0x00000000 =  0 (signed) = 0 (unsigned)
     * ~0 = 0xffffffff = -1 (signed) = UINT_MAX (unsigned)
     * ~1 = 0xfffffffe = -2 (signed)
     * ...
     * ~4 = 0xfffffffb = -5 (signed)
     *
     * donc :
     *
     * ~5 = 0xfffffffa = -6 (signed) = 4294967290 (unsigned)
     *
     * Pour calculer à la main, on fait l'inverse du complément à 2 :
     *
     * ~5 - UINT_MAX - 1 = -6
     *
     */
    x_value = ~5;
    x_value_unsigned = ~5;

    printf("X = ~4\t\t// X = %d (signed)\n", x_value);
    printf("X = ~4\t\t// X = %u (unsigned)\n", x_value_unsigned);

    /*
     * Opérateur booléen (logique) sur un entier, un entier non nul est équivalent à un 1 :
     *
     * !5 = 0 (NON 1 donne 0)
     */
    x_value = !5;

    printf("X = !5\t\t// X = %d\n", x_value);

    return 0;
}
