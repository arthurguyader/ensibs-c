/**
 * \file main.h
 * \brief Boolean and logical operator.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#ifndef __MAIN_H
#define __MAIN_H

#define UINT_MAX 0x10000000

#endif /* __MAIN_H */
