/**
 * \file main.h
 * \brief Sum.
 * \author Baptiste MOINE <contact@bmoine.fr>
 * \author Romain KRAFT <romain.kraft@protonmail.com>
 * \version 0.1-dev
 * \date 10 January 2018
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "main.h"

/**
 * Make some arithmetic operation.
 */
int main(int argc, char *argv[]) {
    char buf[MAX_BUF_LENGTH], continue_;
    int n, i, sum;

    do {
        i = sum = 0;
        n = fget_int("N > ", buf, sizeof(buf));

        while (i <= n) {
            sum += i;
            i++;
        }

        printf("Somme de 0 à %d : %d\n", n, sum);

        continue_ = fget_char("Continue? (y/n)> ", buf, 2);
    } while (continue_ == 'y');

    return 0;
}

/** Get char from standard input.
 *
 * \param message      Message to display to the user for input.
 * \param buf          Buffer used to store user input.
 * \param max_buf_size Maximum buffer size used to store user input.
 *
 * \return char_ Char retrieved from user input.
 */
char fget_char(const char message[], char *buf, const int max_buf_size) {
    int finished = 0;
    char char_;
    char junk_char;

    do {
        printf("%s", message);

        /* Fill the buffer with user input and then convert the buffer to char. */
        if (fgets(buf, max_buf_size, stdin) != NULL) {
            /*
            printf("buf = {");
            for (int i = 0; i < sizeof(buf) - 1; i++) {
                printf("'\\x%x', ", buf[i]);
            }
            printf("'\\%x'};\n", buf[sizeof(buf) - 1]);
            */

            if (sscanf(buf, "%c", &char_) != 1) {  /* Buffer can't be converted to char. */
                fprintf(stderr, "Please enter a valid char!\n");
            } else {
                finished = 1;
            }
        }

        /** Eventually flush the input buffer in order to prevent overflow issues.
         *
         * __Case 1:__ Line break is inside the buffer, e.g. `max_buf_size = 5` and `stdin = '123\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '\n', '\0'};          // ==> {\x31, \x32, \x33, \xa, \0}
         *                                                    //     ^-[&buf]                  ^-[&buf + max_buf_size]
         *                                                    //     ^----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\x0', '\x0', '\x0', '\x0', '\0'};  // ==> {\x0, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 0
         * ```
         *
         * __Case 2:__ Line break is outside the buffer leading to overflow on next buffer, e.g. `max_buf_size = 5` and `stdin = '1234\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '4', '\0'};           // ==> {\x31, \x32, \x33, \x34, \0}
         *                                                    //     ^-[&buf]                   ^-[&buf + max_buf_size]
         *                                                    //     ^-----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\n', '\x0', '\x0', '\x0', '\0'};   // ==> {\xa, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 1
         * ```
         */
        if (buf[strlen(buf) - 1] != 0xa) {  /* Use strlen as index to handle non-full buffer. */
            while ((junk_char = getchar()) != 0xa && junk_char != EOF) {
                /* printf("Flushing! Removing 0x%x from input.\n", junk_char); */
            }
        }
    } while (!finished);

    return char_;
}

/** Get integer from standard input.
 *
 * \param message      Message to display to the user for input.
 * \param buf          Buffer used to store user input.
 * \param max_buf_size Maximum buffer size used to store user input.
 *
 * \return integer Integer number retrieved from user input.
 */
int fget_int(const char message[], char *buf, const int max_buf_size) {
    int integer, finished = 0;
    char junk_char;

    do {
        printf("%s", message);

        /* Fill the buffer with user input and then convert the buffer to integer. */
        if (fgets(buf, max_buf_size, stdin) != NULL) {
            /*
            printf("buf = {");
            for (int i = 0; i < sizeof(buf) - 1; i++) {
                printf("'\\x%x', ", buf[i]);
            }
            printf("'\\%x'};\n", buf[sizeof(buf) - 1]);
            */

            if (sscanf(buf, "%d", &integer) != 1) {  /* Buffer can't be converted to integer. */
                fprintf(stderr, "Please enter a valid integer!\n");
            } else {
                finished = 1;
            }
        }

        /** Eventually flush the input buffer in order to prevent overflow issues.
         *
         * __Case 1:__ Line break is inside the buffer, e.g. `max_buf_size = 5` and `stdin = '123\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '\n', '\0'};          // ==> {\x31, \x32, \x33, \xa, \0}
         *                                                    //     ^-[&buf]                  ^-[&buf + max_buf_size]
         *                                                    //     ^----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\x0', '\x0', '\x0', '\x0', '\0'};  // ==> {\x0, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 0
         * ```
         *
         * __Case 2:__ Line break is outside the buffer leading to overflow on next buffer, e.g. `max_buf_size = 5` and `stdin = '1234\n'`:
         *
         * ```c
         * char buf[] = {'1', '2', '3', '4', '\0'};           // ==> {\x31, \x32, \x33, \x34, \0}
         *                                                    //     ^-[&buf]                   ^-[&buf + max_buf_size]
         *                                                    //     ^-----------[buf]----------^
         *                                                    //
         *                                                    // --> strlen(buf) = 4
         *
         * char buf2[] = {'\n', '\x0', '\x0', '\x0', '\0'};   // ==> {\xa, \x0, \x0, \x0, \0}
         *                                                    //     ^-[&buf2]              ^-[&buf2 + max_buf_size]
         *                                                    //     ^--------[buf2]--------^
         *                                                    //
         *                                                    // --> strlen(buf2) = 1
         * ```
         */
        if (buf[strlen(buf) - 1] != 0xa) {  /* Use strlen as index to handle non-full buffer. */
            while ((junk_char = getchar()) != 0xa && junk_char != EOF) {
                /* printf("Flushing! Removing 0x%x from input.\n", junk_char); */
            }
        }
    } while (!finished);

    return integer;
}
