#include <stdio.h>
#include <time.h>

char* convertTimeToString();

int main(int argc, char *argv[]) {
	printf("%s\n",convertTimeToString());
	return 0;
}

char* convertTimeToString() {
	time_t current_time;
	struct tm *time_info;
	static char time_string[9];

	time(&current_time);
	time_info = localtime(&current_time);
	strftime(time_string,sizeof(time_string),"%H:%M:%S",time_info);

	return time_string;
}
