#ifndef __MAIN_H
#define __MAIN_H

float celsiusAKelvin(float temp);
float celsiusAFahrenheit(float temp);
float kelvinACelsius(float temp);
float fahrenheitACelsius(float temp);
float kelvinAFahrenheit(float temp);
float fahrenheitAKelvin(float temp);

#endif /* __MAIN_H */
