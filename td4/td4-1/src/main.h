/**
Date de naissance (structure DATE):
  Jour: entier
  Mois: entier
  Année: entier
*/
typedef struct{
  int jour;
  int mois;
  int annee;
} Date;



/**
Structure PERSONNE :
  Nom : 20 caractères
  Prénom : 20 caractères
  Date de naissance (structure DATE):
    Jour: entier
    Mois: entier
    Année: entier
*/
typedef struct
{
  char nom[20];
  char prenom [20];
  Date date_de_naissance;
} Personne;
