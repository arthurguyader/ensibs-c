/*
 * TP 5 : Pointeurs
 *
 * Lecture de l'adresse des éléments d'un tableau.
 *
 * Exemple d'exécution avec int t[N] :
 * 
 * t[0] = 10 ; &t[0] = 1595124880
 * t[1] = 20 ; &t[1] = 1595124884
 * t[2] = 30 ; &t[2] = 1595124888
 * t[3] = 40 ; &t[3] = 1595124892
 * t[4] = 50 ; &t[4] = 1595124896
 *
 * On remarque ici un écart de 4 entre chaque addresses, ceci
 * s'explique tout simplement par le fait que la longeur d'un entier est de 4.
 * L'écart changera en fonction du typage des éléments du tableau.
 * 
 *  Adresses                   Mémoire
 * 
 *                  +----------------------------+
 *      0     --->  |             10             |
 *                  |                            |
 *                  +----------------------------+
 *      1     --->  |             20             |
 *                  |                            |
 *                  +----------------------------+
 *      2     --->  |             30             |
 *                  |                            |
 *                  +----------------------------+
 *      3     --->  |             40             |
 *                  |                            |
 *                  +----------------------------+
 *      4     --->  |             50             |
 *                  |                            |
 *                  +----------------------------+
 *                  |             ...            |
 *                  +                            +
 *                  +                            +
 *                  +                            +
 *
 */

#include <stdio.h>
#define N 5

int main(int argc, char *argv[]) {
    int i;
    int t[N] = {10, 20, 30, 40, 50};

    for (i=0; i<N; i++) {
        printf("t[%d] = %d ; &t[%d] = %u\n", i, t[i], i, &t[i]);
    }

    return 0;
}