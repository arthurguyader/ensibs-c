/*
 * TP 5 : Pointeurs
 *
 *  1) création d'un pointeur d'entier, pour le moment il ne pointe sur aucun espace mémoire
 *
 *          px
 *    +------------+
 *    |      ? ---------/
 *    +------------+
 *
 *  2) allocation d'un espace mémoire permettant de stocker un entier
 *
 *          px
 *    +------------+         +------------+
 *    |      +------------------>   ?     |
 *    +------------+         +------------+
 *
 *  3) affectation d'une valeur dans la zone mémoire
 *
 *          px
 *    +------------+         +------------+
 *    |      +------------------>   1     |
 *    +------------+         +------------+
 *
 */

#include <stdio.h>

int main(int argc, char *argv[]) {
    int *px; /* (1) */
    px = (int *) malloc (sizeof(int));  /* (2) */
    *px = 1; /* (3) */

    printf("Adresse de px : %X\n", &px);  /* Affiche l'adresse du pointeur d'entier */
    printf("Adresse pointée par le pointeur px = %X\n", px); /* Affiche l'adresse pointée par le pointeur d'entier */
    printf("Valeur memorisee a l'adresse pointee par le pointeur px = %d\n", *px); /* Affiche la valeur stockée dans l'emplacement mémoire pointé par le pointeur px */

    return 0;
}