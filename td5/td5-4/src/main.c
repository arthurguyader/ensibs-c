/*
 * TP 5 : Pointeurs
 *
 * 1) Creation des pointeurs et allocation d'espaces mémoire pour les entiers :
 *
 *       pi                     i
 * +-----------+          +-----------+
 * |       -----------/   |           |
 * +-----------+          +-----------+
 *
 *       pj                     j
 * +-----------+          +-----------+
 * |       -----------/   |           |
 * +-----------+          +-----------+
 *
 *       pk                     k
 * +-----------+          +-----------+
 * |       -----------/   |           |
 * +-----------+          +-----------+
 *
 *      ptmp                   tmp
 * +-----------+          +-----------+
 * |       -----------/   |           |
 * +-----------+          +-----------+
 *
 * 2) Affectation de valeurs aux entiers :
 *
 *       pi                     i
 * +-----------+          +-----------+
 * |       -----------/   |    31     |
 * +-----------+          +-----------+
 *
 *       pj                     j
 * +-----------+          +-----------+
 * |       -----------/   |    52     |
 * +-----------+          +-----------+
 *
 *       pk                     k
 * +-----------+          +-----------+
 * |       -----------/   |    17     |
 * +-----------+          +-----------+
 *
 *      ptmp                   tmp
 * +-----------+          +-----------+
 * |       -----------/   |           |
 * +-----------+          +-----------+
 *
 * 3) Liaison des pointeurs au espaces mémoire :
 *
 *       pi                     i
 * +-----------+          +-----------+
 * |     +----------------|--> 31     |
 * +-----------+          +-----------+
 *
 *       pj                     j
 * +-----------+          +-----------+
 * |     +-------------------> 52     |
 * +-----------+          +-----------+
 *
 *       pk                     k
 * +-----------+          +-----------+
 * |     +-------------------> 17     |
 * +-----------+          +-----------+
 *
 *      ptmp                   tmp
 * +-----------+          +-----------+
 * |       -----------/   |           |
 * +-----------+          +-----------+
 *
 * 4) Substitution des valeurs dans les espaces mémoire :
 *
 *       pi                     i
 * +-----------+          +-----------+
 * |     +----------------|--> 52     |
 * +-----------+          +-----------+
 *
 *       pj                     j
 * +-----------+          +-----------+
 * |     +-------------------> 17     |
 * +-----------+          +-----------+
 *
 *       pk                     k
 * +-----------+          +-----------+
 * |     +-------------------> 31     |
 * +-----------+          +-----------+
 *
 *      ptmp                   tmp
 * +-----------+          +-----------+
 * |       -----------/   |    31     |
 * +-----------+          +-----------+
 *
 * 5) Substituion des emplacements mémoire entre les pointeurs :
 * 
 *       pi                     i
 * +-----------+          +-----------+
 * |     +--------------+ |    52 <------+
 * +-----------+        | +-----------+  |
 *                      |                |
 *                      |                |
 *       pj             |       j        |
 * +-----------+        | +-----------+  |
 * |     +------------+ +----> 17     |  |
 * +-----------+      |   +-----------+  |
 *                    |                  |
 *                    |                  |
 *       pk           |         k        |
 * +-----------+      |   +-----------+  |
 * |     +----------+ +------> 31     |  |
 * +-----------+    |     +-----------+  |
 *                  |                    |
 *                  |                    |
 *      ptmp        |          tmp       |
 * +-----------+    |     +-----------+  |
 * |       ---------+     |    31     |  |
 * +-----------+    |     +-----------+  |
 *                  |                    |
 *                  +--------------------+
 *
 */

#include <stdio.h>

int main(int argc, char *argv[]) {
    /* (1) */
    int i, j, k, tmp;
    int *pi, *pj, *pk, *ptmp;

    /* (2) */
    i=31;
    j=52;
    k=17;

    /* (3) */
    pi=&i;
    pj=&j;
    pk=&k;

    printf("i=%d j=%d k=%d\n", i, j, k);
    printf("*pi=%d *pj=%d *pk=%d\n", *pi, *pj, *pk);

    /* (4) */
    tmp=*pi;
    *pi=*pj;
    *pj=*pk;
    *pk=tmp;

    printf("i=%d j=%d k=%d tmp=%d\n", i, j, k, tmp);
    printf("*pi=%d *pj=%d *pk=%d\n", *pi, *pj, *pk);

    /* (5) */
    ptmp=pi;
    pi=pj;
    pj=pk;
    pk=ptmp;

    printf("i=%d j=%d k=%d tmp=%d\n", i, j, k, tmp);
    printf("*pi=%d *pj=%d *pk=%d *ptmp=%d\n", *pi, *pj, *pk, *ptmp);

    return 0;
}
