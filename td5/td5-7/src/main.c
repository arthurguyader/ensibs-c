/*
 * TP 5 : Liste chaînées
 *
 * +------+--------------+        +------+--------------+
 * |      |              |        |      |              |        
 * |  1   |      +--------------->|  2   |      +---------------/
 * |      |              |        |      |              |      
 * +------+--------------+        +------+--------------+
 *
 * On créé une structure maillon dans laquelle on place un entier x=1
 * On alloue un nouvel espace mémoire afin d'y stocker un nouveau maillon
 * On place le pointeur de ce nouveau maillon dans l'attribut suivant du précédent maillon (premier maillon ici)
 * On affecte une valeur dans l'attribut x du maillon suivant
 *
 */

#include <stdio.h>
#include <stdlib.h>

#define N 3

typedef struct maillon {
    int x;
    struct maillon * suiv;
} maillon;

int main(int argc, char *argv[]) {
    maillon lc;
    lc.x = 1;
    printf("Valeur du champs x = %d\n\n", lc.x);
    lc.suiv = (maillon *) malloc(sizeof(maillon));
    lc.suiv->x = 2;
    printf("Valeur du champs x du deuxieme maillon = %d\n\n", lc.suiv->x);
    return 0;
}
